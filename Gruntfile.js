module.exports = function (grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        clean: {
            all: ['dist', 'src/styles', 'src/fonts', 'src/vendors', 'style_guide'],
            dev: ['src/styles', 'src/fonts', 'src/vendors'],
            dist: 'dist',
            style_guide: 'style-guide'
        },

        copy: {
            bootstrap_dev: {
                files: [
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/bootstrap/dist/js/bootstrap.min.js',
                        dest: 'src/vendors'
                    },
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/bootstrap/dist/fonts/*.*',
                        dest: 'src/fonts'
                    }
                ]
            },
            font_awesome_dev: {
                files: [
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/font-awesome/css/font-awesome.*',
                        dest: 'src/vendors'
                    },
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/font-awesome/fonts/*.*',
                        dest: 'src/fonts'
                    }
                ]
            },
            jquery_dev: {
                expand: true,
                timestamp: true,
                flatten: true,
                src: 'bower_components/jquery/dist/jquery.min.*',
                dest: 'src/vendors'
            },
            rainbow_dev: {
                files: [
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/rainbow/js/rainbow.min.js',
                        dest: 'src/vendors/rainbow'
                    },
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/rainbow/js/language/generic.js',
                        dest: 'src/vendors/rainbow'
                    },
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/rainbow/js/language/html.js',
                        dest: 'src/vendors/rainbow'
                    },
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/rainbow/themes/github.css',
                        dest: 'src/vendors/rainbow'
                    }
                ]
            },
            application_style_guide: {
                expand: true,
                timestamp: true,
                cwd: 'src',
                src: 'application/**',
                dest: 'dist'
            },
            system_style_guide: {
                expand: true,
                timestamp: true,
                cwd: 'src',
                src: 'system/**',
                dest: 'dist'
            },
            img_style_guide: {
                expand: true,
                timestamp: true,
                cwd: 'src',
                src: 'img/**',
                dest: 'style_guide'
            },
            js_style_guide: {
                expand: true,
                timestamp: true,
                cwd: 'src',
                src: 'js/**',
                dest: 'style_guide'
            },
            bootstrap_style_guide: {
                files: [
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/bootstrap/dist/js/bootstrap.min.js',
                        dest: 'style_guide/vendors'
                    },
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/bootstrap/dist/fonts/*.*',
                        dest: 'style_guide/fonts'
                    }
                ]
            },
            font_awesome_style_guide: {
                files: [
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/font-awesome/css/font-awesome.min.css',
                        dest: 'style_guide/vendors'
                    },
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/font-awesome/fonts/*.*',
                        dest: 'style_guide/fonts'
                    }
                ]
            },
            jquery_style_guide: {
                expand: true,
                timestamp: true,
                flatten: true,
                src: 'bower_components/jquery/dist/jquery.min.js',
                dest: 'style_guide/vendors'
            },
            rainbow_style_guide: {
                files: [
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/rainbow/js/rainbow.min.js',
                        dest: 'style_guide/vendors/rainbow'
                    },
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/rainbow/js/language/generic.js',
                        dest: 'style_guide/vendors/rainbow'
                    },
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/rainbow/js/language/html.js',
                        dest: 'style_guide/vendors/rainbow'
                    },
                    {
                        expand: true,
                        timestamp: true,
                        flatten: true,
                        src: 'bower_components/rainbow/themes/github.css',
                        dest: 'style_guide/vendors/rainbow'
                    }
                ]
            },
            root_style_guide: {
                expand: true,
                timestamp: true,
                flatten: true,
                src: ['src/index.php', 'src/robots.txt'],
                dest: 'style_guide'
            },
            htaccess_style_guide: {
                expand: true,
                timestamp: true,
                src: 'src/.htaccess-test',
                dest: 'style_guide/',
                rename: function(dest, src) {
                    return dest + '.htaccess';
                }
            },
            htpasswd_style_guide: {
                expand: true,
                timestamp: true,
                src: 'src/.htpasswd-test',
                dest: 'style_guide/',
                rename: function(dest, src) {
                    return dest + '.htpasswd';
                }
            }
        },

        less: {
            dev: {
                files: [
                    {
                        options: {
                            strictMath: true,
                            sourceMap: true,
                            outputSourceFiles: true,
                            sourceMapURL: 'bootstrap.css.map',
                            sourceMapFilename: 'src/styles/bootstrap.css.map'
                        },
                        src: 'src/less/bootstrap.less',
                        dest: 'src/styles/bootstrap.css'
                    },
                    {
                        options: {
                            strictMath: true,
                            sourceMap: true,
                            outputSourceFiles: true,
                            sourceMapURL: 'style-guide.css.map',
                            sourceMapFilename: 'src/styles/style-guide.css.map'
                        },
                        src: 'src/less/style-guide.less',
                        dest: 'src/styles/style-guide.css'
                    },
                    {
                        options: {
                            strictMath: true,
                            sourceMap: true,
                            outputSourceFiles: true,
                            sourceMapURL: '<%= pkg.name %>.css.map',
                            sourceMapFilename: 'src/styles/<%= pkg.name %>.css.map'
                        },
                        src: 'src/less/<%= pkg.name %>.less',
                        dest: 'src/styles/<%= pkg.name %>.css'
                    }
                ]
            },
            dist: {
                files: [
                    {
                        options: {
                            strictMath: true,
                            outputSourceFiles: true,
                            compress: true
                        },
                        src: 'src/less/bootstrap.less',
                        dest: 'dist/css/bootstrap.css'
                    },
                    {
                        options: {
                            strictMath: true,
                            outputSourceFiles: true,
                            compress: true
                        },
                        src: 'src/less/<%= pkg.name %>.less',
                        dest: 'dist/css/<%= pkg.name %>.css'
                    }
                ]
            }
        },

        watch: {
            less: {
                files: 'src/less/**/*.less',
                tasks: 'less:dev'
            }
        }
    });

    require('load-grunt-tasks')(grunt, {scope: 'devDependencies'});

    grunt.registerTask('dev', ['clean:dev', 'copy:bootstrap_dev', 'copy:font_awesome_dev', 'copy:jquery_dev', 'copy:rainbow_dev', 'less:dev' ]);
    grunt.registerTask('dist', ['clean:dist', 'less:dist']);
    grunt.registerTask('default', ['dev']);
};