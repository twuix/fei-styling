# FlexNet License Manager

> Application to manage the floating FlexNet licenses

## Getting started

```sh
npm install
bower install
grunt development
grunt watch
```

## Generate distribution

```sh
grunt dist
```
