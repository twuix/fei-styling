<section>
    <h1 id="alerts" class="page-header">Alerts</h1>

    <article>
        <h2 id="alerts-examples">Examples</h2>

        <div class="sample">
            <div class="sample-html">
    <div class="alert alert-success">
        <strong>Well done!</strong> You successfully read this important alert message.
    </div>
    <div class="alert alert-info">
        <strong>Heads up!</strong> This alert needs your attention, but it's not super important.
    </div>
    <div class="alert alert-warning">
        <strong>Warning!</strong> Better check yourself, you're not looking too good.
    </div>
    <div class="alert alert-danger">
        <strong>Oh snap!</strong> Change a few things up and try submitting again.
    </div>
            </div>
        </div>
    </article>

    <article>
        <h2 id="alerts-dismissable-alerts">Dismissable alerts</h2>

        <div class="sample">
            <div class="sample-html">
    <div class="alert alert-warning alert-dismissable">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
        <strong>Warning!</strong> Better check yourself, you're not looking too good.
    </div>
            </div>
        </div>
    </article>

    <article>
        <h2 id="alerts-links-in-alerts">Links in alerts</h2>

        <div class="sample">
            <div class="sample-html">
    <div class="alert alert-success">
        <strong>Well done!</strong> You successfully read <a href="#" class="alert-link">this important alert message</a>.
    </div>
    <div class="alert alert-info">
        <strong>Heads up!</strong> This <a href="#" class="alert-link">alert needs your attention</a>, but it's not important.
    </div>
    <div class="alert alert-warning">
        <strong>Warning!</strong> Better check yourself, you're <a href="#" class="alert-link">not looking too good</a>.
    </div>
    <div class="alert alert-danger">
        <strong>Oh snap!</strong> <a href="#" class="alert-link">Change a few things up</a> and try submitting again.
    </div>
            </div>
        </div>
    </article>
</section>