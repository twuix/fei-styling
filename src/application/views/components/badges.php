<section>
    <h1 id="badges" class="page-header">Badges</h1>

    <article>
        <h2 id="badges-basic">Basic</h2>

        <div class="sample">
            <div class="sample-html">
    <a href="#">Inbox <span class="badge">42</span></a>
            </div>
        </div>
    </article>

    <article>
        <h2 id="badges-examples">Examples</h2>
        <ul class="nav nav-pills">
            <li class="active"><a href="#">Home <span class="badge">42</span></a></li>
            <li><a href="#">Profile</a></li>
            <li><a href="#">Messages <span class="badge">3</span></a></li>
        </ul>
        <br>
        <ul class="nav nav-pills nav-stacked" style="max-width: 260px;">
            <li class="active">
                <a href="#">
                    <span class="badge pull-right">42</span>
                    Home
                </a>
            </li>
            <li><a href="#">Profile</a></li>
            <li>
                <a href="#">
                    <span class="badge pull-right">3</span>
                    Messages
                </a>
            </li>
        </ul>
        <br>
        <button class="btn btn-primary" type="button">
            Messages <span class="badge">4</span>
        </button>
    </article>

    <article>
        <h2 id="badges-colors">Colors</h2>

        <div class="sample">
            <div class="sample-html">
    <span class="badge badge-default">1</span>
    <span class="badge badge-primary">2</span>
    <span class="badge badge-secondary">3</span>
    <span class="badge badge-success">4</span>
    <span class="badge badge-info">5</span>
    <span class="badge badge-warning">6</span>
    <span class="badge badge-danger">7</span>
            </div>
        </div>
    </article>

</section>