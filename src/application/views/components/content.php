<aside class="sidebar">
    <?php $this->load->view('components/sidebar'); ?>
</aside>
<div class="content-view">
    <?php $this->load->view('components/dropdowns'); ?>
    <?php $this->load->view('components/button-groups'); ?>
    <?php $this->load->view('components/button-dropdowns'); ?>
    <?php $this->load->view('components/navs'); ?>
    <?php $this->load->view('components/navbar'); ?>
    <?php $this->load->view('components/breadcrumbs'); ?>
    <?php $this->load->view('components/pagination'); ?>
    <?php $this->load->view('components/labels'); ?>
    <?php $this->load->view('components/badges'); ?>
    <?php $this->load->view('components/page-header'); ?>
    <?php $this->load->view('components/alerts'); ?>

    <?php $this->load->view('components/list-group'); ?>
    <?php $this->load->view('components/panels'); ?>


    <?php $this->load->view('components/wells'); ?>
    <?php $this->load->view('components/modals'); ?>
    <?php $this->load->view('components/tabs'); ?>
    <?php $this->load->view('components/tooltips'); ?>
    <?php $this->load->view('components/popovers'); ?>
</div>