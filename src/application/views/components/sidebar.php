<aside class="sidebar">
    <ul class="list-unstyled">
        <li>
            <a href="#dropdowns" data-toggle="collapse-menu">Dropdowns</a>
            <ul class="collapse">
                <li><a href="#dropdowns-basic">Basic</a></li>
                <li><a href="#dropdowns-headers">Headers</a></li>
                <li><a href="#dropdowns-disabled-menu-items">Disabled menu items</a></li>
            </ul>
        </li>
        <li>
            <a href="#button-groups" data-toggle="collapse-menu">Button groups</a>
            <ul class="collapse">
                <li><a href="#button-groups-basic">Basic</a></li>
                <li><a href="#button-groups-toolbar">Toolbar</a></li>
                <li><a href="#button-groups-sizing">Sizing</a></li>
            </ul>
        </li>
        <li>
            <a href="#button-dropdowns" data-toggle="collapse-menu">Button dropdowns</a>
            <ul class="collapse">
                <li><a href="#button-dropdowns-single">Single</a></li>
                <li><a href="#button-dropdowns-split">Split</a></li>
            </ul>
        </li>
        <li>
            <a href="#navs" data-toggle="collapse-menu">Navs</a>
            <ul class="collapse">
                <li><a href="#navs-tabs">Tabs</a></li>
                <li><a href="#navs-pills">Pills</a></li>
            </ul>
        </li>
        <li>
            <a href="#navbar" data-toggle="collapse-menu">Navbar</a>
            <ul class="collapse">
                <li><a href="#navbar-default">Default</a></li>
                <li><a href="#navbar-inverted">Inverted</a></li>
            </ul>
        </li>
        <li>
            <a href="#breadcrumbs">Breadcrumbs</a>
        </li>
        <li>
            <a href="#pagination" data-toggle="collapse-menu">Pagination</a>
            <ul class="collapse">
                <li><a href="#pagination-default">Default</a></li>
                <li><a href="#pagination-sizing">Sizing</a></li>
            </ul>
        </li>
        <li>
            <a href="#labels" data-toggle="collapse-menu">Labels</a>
            <ul class="collapse">
                <li><a href="#labels-example">Example</a></li>
                <li><a href="#labels-colors">Colors</a></li>
            </ul>
        </li>
        <li>
            <a href="#badges" data-toggle="collapse-menu">Badges</a>
            <ul class="collapse">
                <li><a href="#badges-basic">Basic</a></li>
                <li><a href="#badges-examples">Examples</a></li>
                <li><a href="#badges-colors">Colors</a></li>
            </ul>
        </li>
        <li>
            <a href="#page-header">Page header</a>
        </li>
        <li>
            <a href="#alerts" data-toggle="collapse-menu">Alerts</a>
            <ul class="collapse">
                <li><a href="#alerts-examples">Examples</a></li>
                <li><a href="#alerts-dismissable-alerts">Dismissable alerts</a></li>
                <li><a href="#alerts-links-in-alerts">Links in alerts</a></li>
            </ul>
        </li>
        <li>
            <a href="#list-group" data-toggle="collapse-menu">List group</a>
            <ul class="collapse">
                <li><a href="#list-group-basic-example">Basic example</a></li>
                <li><a href="#list-group-linked-items">Linked items</a></li>
                <li><a href="#list-group-colors">Colors</a></li>
                <li><a href="#list-group-custom-content">Custom content</a></li>
            </ul>
        </li>
        <li>
            <a href="#panels" data-toggle="collapse-menu">Panels</a>
            <ul class="collapse">
                <li><a href="#panels-basic-example">Basic example</a></li>
                <li><a href="#panels-heading">Heading</a></li>
                <li><a href="#panels-footer">Footer</a></li>
                <li><a href="#panels-contextual-alternatives">Contextual alternatives</a></li>
            </ul>
        </li>
        <li>
            <a href="#wells">Wells</a>
        </li>
        <li>
            <a href="#modals">Modals</a>
        </li>
        <li>
            <a href="#tabs">Tabs</a>
        </li>
        <li>
            <a href="#tooltips">Tooltips</a>
        </li>
        <li>
            <a href="#popovers">Popovers</a>
        </li>
    </ul>
</aside>