<section>
    <h1 id="button-groups" class="page-header">Button groups</h1>

    <article>
        <h2 id="button-groups-basic">Basic</h2>

        <div class="sample">
            <div class="sample-html">
    <div class="btn-group" role="group" aria-label="Basic">
        <button type="button" class="btn btn-default">Left</button>
        <button type="button" class="btn btn-default">Middle</button>
        <button type="button" class="btn btn-default">Right</button>
    </div>
            </div>
        </div>
    </article>

    <article>
        <h2 id="button-groups-toolbar">Toolbar</h2>

        <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
            <div class="btn-group" role="group" aria-label="First group">
                <button type="button" class="btn btn-default">1</button>
                <button type="button" class="btn btn-default">2</button>
                <button type="button" class="btn btn-default">3</button>
                <button type="button" class="btn btn-default">4</button>
            </div>
            <div class="btn-group" role="group" aria-label="Second group">
                <button type="button" class="btn btn-default">5</button>
                <button type="button" class="btn btn-default">6</button>
                <button type="button" class="btn btn-default">7</button>
            </div>
            <div class="btn-group" role="group" aria-label="Third group">
                <button type="button" class="btn btn-default">8</button>
            </div>
        </div>

        <pre class="sample-code">
            <code data-language="html">
    <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
        <div class="btn-group" role="group" aria-label="First group">...</div>
        <div class="btn-group" role="group" aria-label="Second group">...</div>
        <div class="btn-group" role="group" aria-label="Third group">...</div>
    </div></code>
        </pre>
    </article>

    <article id="button-groups-sizing-example">
        <h2 id="button-groups-sizing">Sizing</h2>

        <div>
            <div class="btn-group btn-group-lg" role="group" aria-label="Large button group">
                <button type="button" class="btn btn-default">Left</button>
                <button type="button" class="btn btn-default">Middle</button>
                <button type="button" class="btn btn-default">Right</button>
            </div>

            <br>
           <div class="btn-group" role="group" aria-label="Default button group">
                <button type="button" class="btn btn-default">Left</button>
                <button type="button" class="btn btn-default">Middle</button>
                <button type="button" class="btn btn-default">Right</button>
            </div>
            <br>
            <div class="btn-group btn-group-sm" role="group" aria-label="Small button group">
                <button type="button" class="btn btn-default">Left</button>
                <button type="button" class="btn btn-default">Middle</button>
                <button type="button" class="btn btn-default">Right</button>
            </div>
            <br>
            <div class="btn-group btn-group-xs" role="group" aria-label="Extra-small button group">
                <button type="button" class="btn btn-default">Left</button>
                <button type="button" class="btn btn-default">Middle</button>
                <button type="button" class="btn btn-default">Right</button>
            </div>
        </div>

        <pre class="sample-code">
            <code data-language="html">
    <div class="btn-group btn-group-lg" role="group" aria-label="...">...</div>
    <div class="btn-group" role="group" aria-label="...">...</div>
    <div class="btn-group btn-group-sm" role="group" aria-label="...">...</div>
    <div class="btn-group btn-group-xs" role="group" aria-label="...">...</div></code>
        </pre>
    </article>

</section>