<section>
    <h1 id="navs" class="page-header">Navs</h1>

    <article>
        <h2 id="navs-tabs">Tabs</h2>

        <div class="sample">
            <div class="sample-html">
    <ul class="nav nav-tabs">
        <li role="presentation" class="active"><a href="#">Home</a></li>
        <li role="presentation"><a href="#">Profile</a></li>
        <li role="presentation"><a href="#">Messages</a></li>
    </ul>
            </div>
        </div>
    </article>

    <article>
        <h2 id="navs-pills">Pills</h2>

        <div class="sample">
            <div class="sample-html">
    <ul class="nav nav-pills nav-stacked">
        <li role="presentation" class="active"><a href="#">Home</a></li>
        <li role="presentation"><a href="#">Profile</a></li>
        <li role="presentation"><a href="#">Messages</a></li>
    </ul>
            </div>
        </div>
    </article>
</section>