<section>
    <h1 id="breadcrumbs" class="page-header">Breadcrumbs</h1>

    <article>

        <ol class="breadcrumb">
            <li class="active">Home</li>
        </ol>

        <ol class="breadcrumb">
            <li><a href="#">Home</a></li>
            <li class="active">Library</li>
        </ol>

        <div class="sample">
            <div class="sample-html">
    <ol class="breadcrumb">
        <li><a href="#">Home</a></li>
        <li><a href="#">Library</a></li>
        <li class="active">Data</li>
    </ol>
            </div>
        </div>
    </article>
</section>