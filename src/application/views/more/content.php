<aside class="sidebar">
    <?php $this->load->view('more/sidebar'); ?>
</aside>
<div class="content-view">
    <?php $this->load->view('more/flex_table'); ?>
    <?php $this->load->view('more/stat_cards'); ?>
    <?php $this->load->view('more/divided_heading'); ?>
</div>
