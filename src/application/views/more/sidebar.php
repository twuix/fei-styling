<aside class="sidebar">
    <ul class="list-unstyled">
        <li>
            <a href="#flex-table" data-toggle="collapse-menu">Flex table</a>
        </li>
        <li>
            <a href="#stat-cards" data-toggle="collapse-menu">Stat cards</a>
            <ul class="collapse">
                <li><a href="#stat-cards-default">Default</a></li>
                <li><a href="#stat-cards-with-caret">With caret</a></li>
                <li><a href="#stat-cards-with-background">With background</a></li>
            </ul>
        </li>
        <li>
            <a href="#divided-heading" data-toggle="collapse-menu">Divided heading</a>
            <ul class="collapse">
                <li><a href="#divided-heading-example">Example</a></li>
                <li><a href="#divided-heading-nav">Navigation</a></li>
            </ul>
        </li>
    </ul>
</aside>