<aside class="sidebar">
    <ul class="list-unstyled">
        <li>
            <a href="#typography" data-toggle="collapse-menu">Typography</a>
            <ul class="collapse">
                <li><a href="#typography-headings">Headings</a></li>
                <li><a href="#typography-inline-text-elements">Inline text elements</a></li>
            </ul>
        </li>
        <li>
            <a href="#tables" data-toggle="collapse-menu">Tables</a>
            <ul class="collapse">
                <li><a href="#tables-basic">Basic</a></li>
                <li><a href="#tables-striped-rows">Striped rows</a></li>
                <li><a href="#tables-bordered-table">Bordered table</a></li>
                <li><a href="#tables-hover-table">Hover table</a></li>
                <li><a href="#tables-condensed-table">Condensed table</a></li>
            </ul>
        </li>
        <li>
            <a href="#forms" data-toggle="collapse-menu">Forms</a>
            <ul class="collapse">
                <li><a href="#forms-basic">Basic</a></li>
                <li><a href="#forms-horizontal-form">Horizontal form</a></li>
                <li><a href="#forms-supported-controls">Supported controls</a></li>
                <li><a href="#forms-custom-controls">Custom controls</a></li>
                <li><a href="#forms-validation-states">Validation states</a></li>
                <li><a href="#forms-control-sizing">Control sizing</a></li>
            </ul>
        </li>
        <li>
            <a href="#buttons" data-toggle="collapse-menu">Buttons</a>
            <ul class="collapse">
                <li><a href="#buttons-basic">Basic</a></li>
                <li><a href="#buttons-outline">Outline</a></li>
                <li><a href="#buttons-pill">Pill</a></li>
                <li><a href="#buttons-sizes">Sizes</a></li>
                <li><a href="#buttons-states">States</a></li>
                <li><a href="#buttons-button-tags">Button tags</a></li>
            </ul>
        </li>
        <li>
            <a href="#helper-classes" data-toggle="collapse-menu">Helper classes</a>
            <ul class="collapse">
                <li><a href="#helper-classes-contextual-colors">Contextual colors</a></li>
                <li><a href="#helper-classes-contextual-backgrounds">Contextual backgrounds</a></li>
            </ul>
        </li>
    </ul>
</aside>
