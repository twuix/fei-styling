<aside class="sidebar">
    <?php $this->load->view('css/sidebar'); ?>
</aside>
<div class="content-view">
    <?php $this->load->view('css/typography'); ?>
    <?php $this->load->view('css/tables'); ?>
    <?php $this->load->view('css/forms'); ?>
    <?php $this->load->view('css/buttons'); ?>
    <?php $this->load->view('css/helper_classes'); ?>
</div>