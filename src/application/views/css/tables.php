<section>
    <h1 id="tables" class="page-header">Tables</h1>

    <article>
        <h2 id="tables-basic">Basic</h2>

        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>Team</th>
                <th>City</th>
                <th>%V</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Golden State Warriors</td>
                <td>Oakland</td>
                <td>91.0</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Spurs</td>
                <td>San Antonio</td>
                <td>85.3</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Thunder</td>
                <td>Oklahoma</td>
                <td>67.6</td>
            </tr>
            </tbody>
        </table>

        <pre class="sample-code">
            <code data-language="html">
    <table class="table">
    </table></code>
        </pre>
    </article>

    <article>
        <h2 id="tables-striped-rows">Striped rows</h2>

        <table class="table table-striped">
            <thead>
            <tr>
                <th>#</th>
                <th>Team</th>
                <th>City</th>
                <th>%V</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Golden State Warriors</td>
                <td>Oakland</td>
                <td>91.0</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Spurs</td>
                <td>San Antonio</td>
                <td>85.3</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Thunder</td>
                <td>Oklahoma</td>
                <td>67.6</td>
            </tr>
            </tbody>
        </table>

        <pre class="sample-code">
            <code data-language="html">
    <table class="table table-striped">
    </table></code>
        </pre>
    </article>

    <article>
        <h2 id="tables-bordered-table">Bordered table</h2>

        <table class="table table-bordered">
            <thead>
            <tr>
                <th>#</th>
                <th>Team</th>
                <th>City</th>
                <th>%V</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Golden State Warriors</td>
                <td>Oakland</td>
                <td>91.0</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Spurs</td>
                <td>San Antonio</td>
                <td>85.3</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Thunder</td>
                <td>Oklahoma</td>
                <td>67.6</td>
            </tr>
            </tbody>
        </table>

        <pre class="sample-code">
            <code data-language="html">
    <table class="table table-bordered">
    </table></code>
        </pre>
    </article>

    <article>
        <h2 id="tables-hover-table">Hover row</h2>

        <table class="table table-hover">
            <thead>
            <tr>
                <th>#</th>
                <th>Team</th>
                <th>City</th>
                <th>%V</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Golden State Warriors</td>
                <td>Oakland</td>
                <td>91.0</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Spurs</td>
                <td>San Antonio</td>
                <td>85.3</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Thunder</td>
                <td>Oklahoma</td>
                <td>67.6</td>
            </tr>
            </tbody>
        </table>

        <pre class="sample-code">
            <code data-language="html">
    <table class="table table-hover">
    </table></code>
        </pre>
    </article>

    <article>
        <h2 id="tables-condensed-table">Condensed table</h2>

        <table class="table table-condensed">
            <thead>
            <tr>
                <th>#</th>
                <th>Team</th>
                <th>City</th>
                <th>%V</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>Golden State Warriors</td>
                <td>Oakland</td>
                <td>91.0</td>
            </tr>
            <tr>
                <td>2</td>
                <td>Spurs</td>
                <td>San Antonio</td>
                <td>85.3</td>
            </tr>
            <tr>
                <td>3</td>
                <td>Thunder</td>
                <td>Oklahoma</td>
                <td>67.6</td>
            </tr>
            </tbody>
        </table>

        <pre class="sample-code">
            <code data-language="html">
    <table class="table table-condensed">
    </table></code>
        </pre>
    </article>
</section>