<section>
    <h1 id="forms" class="page-header">Forms</h1>

    <article>
        <h2 id="forms-basic">Basic</h2>

        <div class="sample">
            <div class="sample-html">
    <form role="form">
        <div class="form-group">
            <label for="exampleInputEmail1">Email address</label>
            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword1">Password</label>
            <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Password">
            <p class="help-block">Example block-level help text here.</p>
        </div>
        <div class="form-group">
            <label for="exampleInputRequest">Request</label>
            <textarea class="form-control" rows="3"></textarea>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>
            </div>
        </div>
    </article>

    <article>
        <h2 id="forms-horizontal-form">Horizontal form</h2>

        <div class="sample">
            <div class="sample-html">
    <form class="form-horizontal" role="form">
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
            <div class="col-sm-10">
                <p class="form-control-static">email@example.com</p>
            </div>
        </div>
        <div class="form-group">
            <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
            <div class="col-sm-10">
                <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <div class="checkbox">
                    <label>
                        <input type="checkbox"> Remember me
                    </label>
                </div>
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">Sign in</button>
            </div>
        </div>
    </form>
            </div>
        </div>
    </article>

    <article>
        <h2 id="forms-supported-controls">Supported controls</h2>

        <h3>Input</h3>

        <div class="sample">
            <div class="sample-html">
    <input type="text" class="form-control" placeholder="Text input">
            </div>
        </div>

        <h3>Icon input</h3>

        <div class="sample">
            <div class="sample-html">
    <div class="input-with-icon">
        <input type="text" class="form-control" placeholder="Text input">
        <span class="fa fa-calendar"></span>
    </div>
            </div>
        </div>

        <h3>Textarea</h3>

        <div class="sample">
            <div class="sample-html">
    <textarea class="form-control" rows="3"></textarea>
            </div>
        </div>
    </article>

    <article>
        <h2 id="forms-validation-states">Validation states</h2>

        <h3>Basic</h3>

        <div class="sample">
            <div class="sample-html">
    <div class="form-group has-success">
        <label class="control-label" for="inputSuccess1">Input with success</label>
        <input type="text" class="form-control" id="inputSuccess1">
    </div>
    <div class="form-group has-warning">
        <label class="control-label" for="inputWarning1">Input with warning</label>
        <input type="text" class="form-control" id="inputWarning1">
    </div>
    <div class="form-group has-error">
        <label class="control-label" for="inputError1">Input with error</label>
        <input type="text" class="form-control" id="inputError1">
    </div>
            </div>
        </div>

        <h3>With icons</h3>

        <div class="sample">
            <div class="sample-html">
    <div class="form-group has-success has-feedback">
        <label class="control-label" for="inputSuccess2">Input with success</label>
        <input type="text" class="form-control" id="inputSuccess2">
        <span class="glyphicon glyphicon-ok form-control-feedback"></span>
    </div>
    <div class="form-group has-warning has-feedback">
        <label class="control-label" for="inputWarning2">Input with warning</label>
        <input type="text" class="form-control" id="inputWarning2">
        <span class="glyphicon glyphicon-warning-sign form-control-feedback"></span>
    </div>
    <div class="form-group has-error has-feedback">
        <label class="control-label" for="inputError2">Input with error</label>
        <input type="text" class="form-control" id="inputError2">
        <span class="glyphicon glyphicon-remove form-control-feedback"></span>
    </div>
            </div>
        </div>
    </article>

    <article>
        <h2 id="forms-control-sizing">Control sizing</h2>

        <div class="sample">
            <div class="sample-html sample-control-sizing">
    <input type="text" class="form-control input-lg" placeholder=".input-lg">
    <input type="text" class="form-control" placeholder="Default input">
    <input type="text" class="form-control input-sm" placeholder=".input-sm">
            </div>
        </div>
    </article>
</section>